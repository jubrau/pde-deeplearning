import json
import numpy as np

import charateristics_utils
from example_functions import initial_func3

good_values1 = {'a': [],
                'b': [],
                'bt': []}
good_values2 = {'a': [],
                'b': [],
                'bt': []}
good_values3 = {'a': [],
                'b': [],
                'c': [],
                'bt': []}
# for b in np.arange(1, 5, 0.5):
#     for a in np.arange(b + 0.5, 10, 0.5):
#         bt = charateristics_utils.breaking_time(initial_func1(a, b), left=0, right=1)
#         if bt > 0.2:
#             good_values1['a'].append(a)
#             good_values1['b'].append(b)
#             good_values1['bt'].append(bt)
# for b in [1, 2]:
#     if b == 1:
#         a_range = np.arange(-1, 0, 0.05)
#     else:
#         a_range = np.arange(0.02, 0.2, 0.02)
#     for a in a_range:
#         bt = charateristics_utils.breaking_time(initial_func2(a, b), left=0, right=1)
#         if bt > 0.2:
#             good_values2['a'].append(a)
#             good_values2['b'].append(b)
#             good_values2['bt'].append(bt)
for b in [1, 2]:
    for a in np.arange(2, 20, 1):
        for c in np.arange(a, 40, 2):
            bt = charateristics_utils.breaking_time(initial_func3(a, b, c), left=0.0, right=1.0)
            if bt is not None and bt > 0.2:
                good_values3['a'].append(float(a))
                good_values3['b'].append(float(b))
                good_values3['c'].append(float(c))
                good_values3['bt'].append(bt)
        print('a')
    print('Halfway done')

# with open('data/parameters1.json', 'w') as f:
#     json.dump(good_values1, f)
#
# with open('data/parameters2.json', 'w') as f:
#     json.dump(good_values2, f)

with open('data/parameters3.json', 'w') as f:
    json.dump(good_values3, f)
