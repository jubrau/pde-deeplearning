import numpy as np
from periodic_data_utils import dx_derivative
from pynverse import inversefunc
from scipy.interpolate import interp1d
import grid_utils


def modify_boundary(data, value=1, axis=1):
    """
    Replace the value of the last element along axis by value.
    :param data: numpy array
    :param value: value to replace with
    :param axis: axis along with to replace
    :return: modified data
    """
    slc = [slice(None)] * data.ndim
    slc[axis] = -1
    data[tuple(slc)] = value
    return data


def calculate_coefficients(solution, phi, x_vals):
    """
    Calculate the coefficients of the solution with respect to the basis transformed by phi,
    using hat basis functions centered at x_vals.
    :param solution: solution (wrt hat basis functions), shape (num_t_vals, num_x_vals (,1))
    :param phi: values of diffeomorphism at x_vals (num_t_vals, num_x_vals (,1))
    :param x_vals: x values
    :return: coefficients
    """
    solution = np.squeeze(solution)
    x_vals = np.squeeze(x_vals)
    phi = np.squeeze(phi)
    phi_modified = modify_boundary(phi, value=1, axis=1)
    phi_inverse = [inversefunc(interp1d(x_vals, phi_modified[t, :]), y_values=x_vals, domain=[0, 1]) for t in
                   range(phi.shape[0] // 20)]
    shifted_solutions = [interp1d(x_vals[:], solution[t, :])(phi_inverse[t]) for t in
                         range(len(phi_inverse))]
    shifted_solutions = np.asarray(shifted_solutions)
    return shifted_solutions


def calculate_new_mass_matrix(phi, x_vals):
    dx = x_vals[1] - x_vals[0]
    # original hat function basis vectors
    original_basis_vectors = [grid_utils.p1_function(center, dx)(x_vals) for center in x_vals]
    original_basis_vectors = np.squeeze(np.asarray(original_basis_vectors))
    phi_modified = modify_boundary(phi, value=1, axis=1)
    phi_derivative = dx_derivative(phi_modified[:, :, 0], dx, x_mode='central')
    mass_matrix = np.einsum('rjk, sjk -> jrs',
                            original_basis_vectors[1:-1, np.newaxis, 1:-1] / phi_derivative[np.newaxis, :, 1:-1],
                            original_basis_vectors[1:-1, np.newaxis, 1:-1]) * dx
    return mass_matrix
