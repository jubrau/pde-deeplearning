from example_functions import initial_func1
import numpy as np
from periodic_data_utils import dx_derivative
import matplotlib.pyplot as plt
import json

good_values = {'a': [],
               'b': [],
               'bt': []}

for b in [1, 2, 3]:
    for a in np.arange(2, 40, 2):
        u0 = initial_func1(a, b, numpy=True, scale=2, left=0, right=0.5, extended=True)(np.linspace(0, 1, 1000))
        derivatives = dx_derivative(u0[np.newaxis, :], 1. / 1000)
        bt = - 1. / np.min(derivatives)
        if bt > 0.20:
            good_values['a'].append(float(a))
            good_values['b'].append(float(b))
            good_values['bt'].append(bt)
        # plt.plot(u0, label=round(bt, 2))
        # plt.legend()
    # plt.show(block=True)
    # plt.clf()

with open('data/parameters1_differently.json', 'w') as f:
    json.dump(good_values, f)
