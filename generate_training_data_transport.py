from charateristics_utils import char_curve_values, char_curve_inv_values
from periodic_data_utils import transport_solution
import numpy as np
from example_functions import initial_func1, initial_func2, initial_func3, numpy_version
import json
from functools import wraps
import pandas as pd

with open('data/parameters1.json', 'r') as f:
    parameters1 = json.load(f)
with open('data/parameters2.json', 'r') as f:
    parameters2 = json.load(f)
with open('data/parameters3_differently.json', 'r') as f:
    parameters3 = json.load(f)

dt = 0.001
num_x_vals = 1000


def inform_when_done(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        result = func(*args, **kwargs)
        print(f'Done executing {func.__name__} with parameters {args} and {kwargs}.')
        return result

    return wrapped


t_values = np.arange(0, 0.2, dt)
x_values = np.linspace(0, 1, num_x_vals + 1)

labels = ['solution', 'x_values', 't_values', 'characteristics_inverse', 'characteristics']


def solve(initial_func):
    @wraps(initial_func)
    @inform_when_done
    def solver_function(**parameters):
        # function with parameters x, **kwargs
        initial_func_np = numpy_version(initial_func)
        u, characteristics, characteristics_inverse = transport_solution(initial_func_np, x_values, t_values,
                                                                         **parameters)
        return (u, x_values[:, np.newaxis], t_values, characteristics_inverse, characteristics,
                *[parameters[key] for key in sorted(parameters.keys())])

    return solver_function


results = []
for a, b in zip(parameters1['a'], parameters1['b']):
    solution = solve(initial_func1)(a=a, b=b)
    results.append(solution)
parameters1 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b']).to_dict(orient='list')
np.save('data/transport_equation_1.npy', parameters1)

results = []
for a, b in zip(parameters2['a'], parameters2['b']):
    solution = solve(initial_func2)(a=a, b=b)
    results.append(solution)
parameters2 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b']).to_dict(orient='list')
np.save('data/transport_equation_2.npy', parameters2)

results = []
for a, b, c in zip(parameters3['a'], parameters3['b'], parameters3['c']):
    solution = solve(initial_func3)(a=a, b=b, c=c)
    results.append(solution)
parameters3 = pd.DataFrame().from_records(results, columns=labels + ['a', 'b', 'c']).to_dict(orient='list')
np.save('data/transport_equation_3.npy', parameters3)
