import grid_utils
import numpy as np
from nn_architectures import PdeRNN
import pandas as pd
# import fenics
import scipy
import scipy.sparse.linalg as linalg
import plotting_utils
import matplotlib.pyplot as plt
from pynverse import inversefunc
from periodic_data_utils import dx_derivative
from scipy.interpolate import interp1d

dataset1 = pd.DataFrame(np.load('data/parameters1_precise.npy').item())
dataset2 = pd.DataFrame(np.load('data/parameters2_precise.npy').item())
dataset3 = pd.DataFrame(np.load('data/parameters3_precise.npy').item()).drop('c', 1)
dataset = pd.concat([dataset1, dataset3, dataset2]).to_dict(orient='list')

num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
t_vals = np.asarray(dataset['t_values'][0])
x_vals = np.asarray(dataset['x_values'][0])

t_vals_broadcasted = np.broadcast_to(np.asarray(t_vals)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]
dt = dataset['t_values'][0][1] - dataset['t_values'][0][0]
solutions = [np.asarray(solution).T[:, :, np.newaxis] for solution in dataset['solution']]

file_suffix = 'precise'

pde_rnn = PdeRNN(num_t_vals, parameter_units=200, input_units=60, dt=dt, dx=dx, num_parameters=0,
                 num_parameters_depending_on_t=1)
pde_rnn.build(activation='relu')
model = pde_rnn.model

num_elements_old_basis = num_x_vals
num_elements_new_basis = num_x_vals

model.load_weights(f'results/withutx/weights_{file_suffix}.h5')
# result: phi(t, x) for all x, t, shape: (num_t_vals-1, num_x_vals, 1)
result = np.asarray(model.predict([solutions[0], t_vals_broadcasted, x_vals]))
# original hat function basis vectors
original_basis_vectors = [grid_utils.p1_function(center, dx)(x_vals) for center in x_vals]
original_basis_vectors = np.squeeze(np.asarray(original_basis_vectors))
# basis vectors: B_i(phi(t,x)) for all i (num_elements_new_basis)
basis_vectors = [grid_utils.p1_function(center, dx)(result) for center in x_vals]
basis_vectors = np.squeeze(np.asarray(basis_vectors))
# shape: (num_elements_new_basis, num_t_vals-1, num_x_vals)

# replace last value by 1.0
result_modified = np.concatenate((result[:, :-1], np.ones(shape=(result.shape[0], 1, 1))), axis=1)
phi_derivative = dx_derivative(result[:, :, 0], dx, x_mode='central')
# phi_funs = [interp1d(x_vals[:,0], result_modified[t, :, 0]) for t in range(result.shape[0])]
phi_inverse = [inversefunc(interp1d(x_vals[:, 0], result_modified[t, :, 0]), y_values=x_vals, domain=[0, 1]) for t in
               range(result.shape[0])]
mass_matrix = np.einsum('rjk, sjk -> jrs',
                        original_basis_vectors[1:-1, np.newaxis, 1:-1] / phi_derivative[np.newaxis, :, 1:-1],
                        original_basis_vectors[1:-1, np.newaxis, 1:-1]) * dx

# actually these are already the coefficients we look for
shifted_solutions = [interp1d(x_vals[:, 0], solutions[0][:, t, 0])(phi_inverse[t][:, 0]) for t in
                     range(len(phi_inverse))]

# calculate load vector (j = index of new basis function)
load_vector = np.einsum('jtk,kt->jt',
                        original_basis_vectors[1:-1, np.newaxis, 1:-1] / phi_derivative[np.newaxis, :, 1:-1],
                        shifted_solutions[0][1:-1, :, 0])

# # t = 20
# mesh = fenics.IntervalMesh(num_x_vals - 1, 0, 1)
# V = fenics.FunctionSpace(mesh, 'P', 1)
# u = fenics.TrialFunction(V)
# v = fenics.TestFunction(V)
# A = fenics.assemble(u * v * fenics.dx()).array()
# # coordinates transformed basis w.r.t. old basis
# x = linalg.spsolve(scipy.sparse.csr_matrix(A), load_vectors)
# x = x.T
# x = np.reshape(x, (num_elements_new_basis, num_t_vals - 1, num_elements_old_basis))
# # x now has shape (num_t_vals-1, num_elements_old_basis, num_elements_new_basis)
# x = np.transpose(x, [1, 2, 0])

gammas = []
for t in range(num_t_vals - 1):
    gamma = linalg.spsolve(scipy.sparse.csr_matrix(mass_matrix[t]), load_vector[:, t])
    gammas.append(gamma)
    print(t)
