from fenics import *

num_elements = 200

mesh = IntervalMesh(num_elements, -1, 1)

class PeriodicBoundary(SubDomain):

    def inside(self, x, on_boundary):
        return bool(-DOLFIN_EPS -1 < x[0] < -1 + DOLFIN_EPS and on_boundary)

    def map(self, x, y):
        y[0] = x[0] - 2
        print(x)


V = FunctionSpace(mesh, 'P', 1, constrained_domain=PeriodicBoundary())

u = TrialFunction(V)

A = assemble(u*dx)
B = assemble(grad(u)[0]*dx)
