import numpy as np

from nn_architectures import plot_train_function

dataset1 = np.load('data/parameters1.npy').item()
dataset2 = np.load('data/parameters2.npy').item()
dataset3 = np.load('data/parameters3_old.npy').item()

dataset = dataset3

num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
t_values = [np.broadcast_to(np.asarray(t_value)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
            for t_value in dataset['t_values']]
initial_functions = [np.asarray(u[0]) for u in dataset['sol']]
dataset_new = {'t_values': t_values,
               'parameters': initial_functions,
               'characteristics_inv': dataset['characteristics_inv'],
               'x_values': dataset['x_values'],
               'initial_functions': initial_functions}
for key, value in dataset_new.items():
    if key is not 'characteristics_inv':
        dataset_new[key] = np.concatenate(value, axis=0)

plot_train_function(0.5, dataset_new, t_range=slice(10, None, None))
