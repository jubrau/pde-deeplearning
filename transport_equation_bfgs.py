import numpy as np
from periodic_data_utils import periodic_coordinates, extend_function, generate_derivatives
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.keras.layers import Dense
from itertools import product
import pandas as pd
from collections import defaultdict
from sklearn.model_selection import train_test_split


def generate_data(f, x_vals, t_vals, velocity=1, wrap=1, a=2, b=2):
    xv, tv = np.meshgrid(x_vals, t_vals)
    coordinates = periodic_coordinates(xv - velocity * tv, wrap)
    f = extend_function(f, 0, 1)
    data = f(coordinates, a, b)
    return data


dx = 0.001
dt = 0.0001
end_x = 3
x_vals = np.arange(0, end_x, dx)
t_vals = np.arange(0, 0.001, dt)
num_x_vals = len(x_vals)
num_t_vals = len(t_vals)


def f(x, a=2, b=2):
    return np.sin(a * np.pi * x) * (np.cos(2 * np.pi * x) - 1) ** b
    # return (np.cos(2 * np.pi * x) - 1)**2


# sanity check
def dx_f_fun(x, a=2, b=2):
    # return a*np.pi*(np.cos(2*np.pi*x)-1)**b - np.sin(a*np.pi*x)*b*(np.cos(2*np.pi*x)-1)**(b-1)*np.sin(2*np.pi*x)*2*np.pi
    return -4 * np.pi * np.sin(2 * np.pi * x) * (np.cos(2 * np.pi * x) - 1)


data = generate_data(f, x_vals, t_vals, 1, wrap=3)


# generate a training set consisting of u, dt_u, dt_x and parameters
# kwargs are arguments that f takes, which is the function generating u
def generate_dataset(f, x_vals, t_vals, dt, dx, wrap=1, **kwargs):
    training_data = defaultdict(list)
    keys = kwargs.keys()
    values = kwargs.values()
    for params in product(*list(values)):
        u = generate_data(f, x_vals, t_vals, wrap=wrap, **dict(zip(keys, params)))
        dt_u, dx_u = generate_derivatives(u, dt, dx)
        training_data['u'].append(u)
        training_data['dt_u'].append(dt_u)
        training_data['dx_u'].append(dx_u)
        for key, param in zip(keys, params):
            training_data[key].append(np.array(param, ndmin=1))
    return training_data


# b = 2
# for a in np.arange(1, 5, 1):
#     for velocity in np.arange(1, 10, 0.5):
#         u = generate_data(f, x_vals=x_vals, t_vals=t_vals, velocity=velocity, wrap=3, a=a, b=b)
#         dt_u, dx_u = generate_derivatives(u, dt, dx)
#         training_data['u'].append(u)
#         training_data['dt_u'].append(dt_u)
#         training_data['dx_u'].append(dx_u)
#         training_data['a'].append(np.array(a, ndmin=1))
#         training_data['b'].append(np.array(b, ndmin=1))
#         training_data['velocity'].append(np.array(velocity, ndmin=1))


dataset = generate_dataset(f, x_vals, t_vals, dt, dx, a=np.arange(1, 5, 1), b=[2],
                           velocity=np.arange(1, 10, 0.5))
dataset_df = pd.DataFrame(dataset)

train, test = train_test_split(dataset_df, test_size=0.33, random_state=42, shuffle=True)

# sanity check
diff = []
xv, tv = np.meshgrid(x_vals, t_vals)
for i in range(6):
    dt_u = dataset['dt_u'][i][1:-1, :]
    dx_u = dataset['dx_u'][i][1:-1, :]
    u = dataset['u'][i]
    v = dataset['velocity'][i]
    a = dataset['a'][i]
    b = dataset['b'][i]
    coordinates = periodic_coordinates(xv - v * tv, 3)
    dx_f = extend_function(dx_f_fun, 0, 1)(coordinates, a, b)
    diff.append(np.abs(dt_u + v * dx_u))

# start to define neural network
input_dx_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_x_vals, 1))
input_dt_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_x_vals, 1))
input_velocity = tf.placeholder(dtype='float', shape=(1, 1))
input_x = tf.constant(value=x_vals, dtype='float', shape=(num_x_vals, 1))
# var_c = tf.Variable(initial_value=0, trainable=True, name='c', dtype='float')

phi_xt = input_x
phi_values = [phi_xt]
loss = 0

layer = Dense(10, activation='relu', use_bias=True)(input_velocity)
var_c = Dense(1, activation=None, kernel_initializer=tf.keras.initializers.Constant(1), use_bias=True)(layer)

for i in range(1, num_t_vals):
    phi_xt = phi_xt + dt * var_c
    phi_values.append(phi_xt)
    loss += tf.reduce_sum((input_dt_u[i] + var_c * input_dx_u[i]) ** 2)

# training loop
with tf.Session() as sess:
    tf.global_variables_initializer().run()
    nb_epochs = 1500
    c_vals = np.zeros((nb_epochs, 1))
    current_c = 0
    losses = []
    test_losses = []


    def loss_fun(variables):
        return loss, tf.gradients(loss, tf.Tensor(tf.trainable_variables()))


    test_feed_dicts = [{input_dx_u: np.expand_dims(test['dx_u'].iloc[j], -1),
                        input_dt_u: np.expand_dims(test['dt_u'].iloc[j], -1),
                        input_velocity: np.expand_dims(test['velocity'].iloc[j], -1)
                        } for j in range(len(test['u']))]
    start = np.arange(10, 0, -1, dtype='float64')
    optim_results = tfp.optimizer.lbfgs_minimize(
        loss_fun, initial_position=[var.eval() for var in tf.trainable_variables()], num_correction_pairs=10,
        tolerance=1e-8)
    res = sess.run(optim_results, feed_dict=test_feed_dicts[0])
    for i in range(nb_epochs):
        epoch_loss = 0
        for j in np.random.permutation(len(train['u'])):
            feed_dict = {input_dx_u: np.expand_dims(train['dx_u'].iloc[j], -1),
                         input_dt_u: np.expand_dims(train['dt_u'].iloc[j], -1),
                         input_velocity: np.expand_dims(train['velocity'].iloc[j], -1)
                         }

            epoch_loss += current_loss
            # print("Loss:" + str(current_loss))
            # print("C:" + str(current_c))
        c_vals[i] = current_c
        losses.append(epoch_loss / len(dataset['u']))
        test_loss = 0
        for k in range(len(test_feed_dicts)):
            test_loss += sess.run(loss, feed_dict=test_feed_dicts[k])
        test_losses.append(test_loss / len(test_feed_dicts))
        # print(current_c)

# plt.plot(np.arange(nb_epochs), c_vals)

# plt.ylim(top=10)
plt.plot(losses[20:])
plt.plot(test_losses[20:])
plt.show()
