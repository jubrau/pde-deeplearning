import numpy as np
from itertools import product
from collections import defaultdict
import sympy
from scipy import ndimage


# add periodic boundary data to data
def periodify_data(u):
    first_column = u[:, 0]
    last_column = u[:, -1]
    u = np.concatenate((u, np.expand_dims(first_column, -1)), axis=-1)
    u = np.concatenate((np.expand_dims(last_column, -1), u), axis=-1)
    # first_row = u[0, :]
    # last_row = u[-1, :]
    # u = np.concatenate((u, np.expand_dims(first_row, 0)), axis=0)
    # u = np.concatenate((np.expand_dims(last_row, 0), u), axis=0)
    return u


# shape of u: (t,x)
def dx_derivative(u, dx, x_mode='forward'):
    x_kernel = get_diff_kernel(x_mode, dx)
    data_x = ndimage.correlate(u, x_kernel, mode='wrap')
    return data_x


# shape of u: (t,x)
def dt_derivative(u, dt, t_mode='forward'):
    t_kernel = get_diff_kernel(t_mode, dt)
    data_t = ndimage.correlate(u, t_kernel.T, mode='wrap')
    return data_t


# generate derivatives using periodic boundary values
# shape of u: (t,x)
def generate_derivatives(u, dt, dx, t_mode='forward', x_mode='forward'):
    data_t = dt_derivative(u, dt, t_mode)
    data_x = dx_derivative(u, dx, x_mode)
    return data_t, data_x


# calculate periodic coordinates by modulo, such that they are in [0,wrap)
def periodic_coordinates(coordinates, wrap):
    coordinates = coordinates % wrap
    return coordinates


# takes a function of one spatial argument and optional parameter, and returns a function which is
# equal to this function between left and right and zero otherwise
def extend_function(f, left, right, value=0):
    return lambda x, *args, **kwargs: np.where(np.logical_and(x <= right, x >= left), f(x, *args, **kwargs), value)


def extend_function_sympy(f, x, left, right, value=0):
    return sympy.Piecewise((f, (x < right) & (x > left)), (value, True))


def transport_solution(f, x_vals, t_vals, velocity=1, wrap=1, **parameters):
    xv, tv = np.meshgrid(x_vals, t_vals)
    coordinates = periodic_coordinates(xv - velocity * tv, wrap)
    f_extended = extend_function(f, 0, 1)
    data = f_extended(coordinates, **parameters)
    characteristics_inverse = xv - velocity * tv
    characteristics = periodic_coordinates(xv + velocity * tv, wrap)
    return data, characteristics, characteristics_inverse


# generate a training set consisting of u, dt_u, dt_x and parameters
# kwargs are parameters that gen_fun takes, which is the function generating u
def generate_solutions_to_transport_equation(initial_function, x_vals, t_vals, dt, dx, wrap=1, **kwargs):
    training_data = defaultdict(list)
    keys = kwargs.keys()
    values = kwargs.values()
    for params in product(*list(values)):
        u = transport_solution(initial_function, x_vals, t_vals, wrap=wrap, **dict(zip(keys, params)))[0]
        dt_u, dx_u = generate_derivatives(u, dt, dx)
        training_data['u'].append(u)
        training_data['dt_u'].append(dt_u)
        training_data['dx_u'].append(dx_u)
        for key, param in zip(keys, params):
            training_data[key].append(np.repeat(param, repeats=len(x_vals)))
    return training_data


# [-1, 1, 0] = backward
# [0, -1, 1] = forward
# [-1, 0, 1] = central
def get_diff_kernel(mode, delta=1):
    if mode == 'forward':
        kernel = np.array([0, -1, 1], ndmin=2) / delta
    elif mode == 'backward':
        kernel = np.array([-1, 1, 0], ndmin=2) / delta
    elif mode == 'central':
        kernel = np.array([-1, 0, 1], ndmin=2) / (2 * delta)
    elif mode == 'so-central':
        kernel = np.array([1, -2, 1], ndmin=2) / (delta ** 2)
    elif mode == 'so-forward':
        kernel = np.array([0, 0, 1, -2, 1], ndmin=2) / (delta ** 2)
    elif mode == 'so-backward':
        kernel = np.array([1, -2, 1, 0, 0], ndmin=2) / (delta ** 2)
    else:
        kernel = None
    return kernel
