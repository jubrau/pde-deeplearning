from pymor.basic import *
from example_functions import initial_func3, pymor_version
import json
import numpy as np
from charateristics_utils import char_curve_inv_values, char_curve_values


# define problem to solve
def burgers_problem_parametric_initial_data(initial_data=None, v=1., circle=True):
    """One-dimensional Burgers-type problem.

    The problem is to solve ::

        ∂_t u(x, t, μ)  +  ∂_x (v * u(x, t, μ)^μ_1) = 0
                                       u(x, 0, μ) = u_0(x, μ_2)

    for u with t in [0, 0.3] and x in [0, 2].

    Parameters
    ----------
    v
        The velocity v.
    circle
        If `True`, impose periodic boundary conditions. Otherwise Dirichlet left,
        outflow right.
    parameter_range
        The interval in which μ is allowed to vary.
    """
    return InstationaryProblem(

        StationaryProblem(
            domain=CircleDomain([0, 2]) if circle else LineDomain([0, 2], right=None),

            dirichlet_data=None,

            rhs=None,

            nonlinear_advection=ExpressionFunction('abs(x)**exponent * v',
                                                   1, (1,), {'exponent': ()}, {'v': v}),

            nonlinear_advection_derivative=ExpressionFunction('exponent * abs(x)**(exponent-1) * sign(x) * v',
                                                              1, (1,), {'exponent': ()}, {'v': v}),
        ),

        T=0.3,

        initial_data=initial_data,

        name="burgers_problem({}, {})".format(v, circle)
    )


with open('data/parameters3_differently.json', 'r') as f:
    parameters3 = json.load(f)

T = 0.8
p = burgers_problem_parametric_initial_data()
p = p.with_(domain=CircleDomain([0., 1.]),
            initial_data=GenericFunction(pymor_version(initial_func3, extended=True, left=0, right=0.5, scale=2),
                                         parameter_type={'a': 0, 'b': 0, 'c': 0}), T=T)

# discretize problem with upwind FV and explicit Euler timestepping
dx = 1 / 1000
nt = 3000
t_values = np.linspace(0, T, nt, endpoint=True)
x_values = np.arange(0, 1, dx)
d, data = discretize_instationary_fv(p, num_flux='simplified_engquist_osher', diameter=dx, nt=nt)
U = d.solve(Parameter({'exponent': 2, 'a': 10, 'b': 1, 'c': -20}))
d.visualize(U, title='solution')

#
# i = 0
# for exponent in [1, 1.5, 2]:
#     for a, b, c, bt in zip(parameters3['a'], parameters3['b'], parameters3['c'], parameters3['bt']):
#         # solve
#         U = d.solve(Parameter({'exponent': exponent, 'a': a, 'b': b, 'c': c}))
#         # convert to NumPy array, first index time, second index space
#         t_values_for_characteristics = t_values[::nt // 100]
#         U_numpy = U.to_numpy()
#         if exponent != 1:
#             characteristics = char_curve_values(
#                 initial_func3(a=a, b=b, c=c, numpy=True, extended=True, left=0, right=0.5, scale=2),
#                 t_values_for_characteristics, x_values, nonlinear_func=lambda x: x ** (exponent - 1))
#             characteristics_inverse = char_curve_inv_values(
#                 initial_func3(a=a, b=b, c=c, numpy=True, extended=True, left=0, right=0.5, scale=2),
#                 t_values_for_characteristics[t_values_for_characteristics < bt],
#                 x_values[x_values < 0.5], domain=[0, 0.5], nonlinear_func=lambda x: x ** (exponent - 1))
#
#         generated_data = {'solution': U_numpy.tolist(), 'a': a, 'b': b, 'bt': bt, 'dx': dx, 'nt': nt, 'T': T}
#         with open('data/single_files/single_files_2/data_{:04d}.json'.format(i), 'w') as f:
#             json.dump(generated_data, f)
#         i += 1
