from fenics import *
import matplotlib.pyplot as plt
import numpy as np

t0 = 0
num_elements = 500
p_nu = 1
p_eps = 1
dt = 0.001

mesh = IntervalMesh(num_elements, 0, 1)
V = FunctionSpace(mesh, 'P', 1)


def boundary(x, on_boundary):
    return on_boundary


u = Function(V)
v = TestFunction(V)

u0 = Expression("2*x[0]", degree=1)
u0 = project(u0, V)

F = u*v*dx + dt*p_nu*grad(u)[0]*v*u*dx + dt*p_eps*inner(grad(u), grad(v))*dx - u0*v*dx
#F = u*v*dx + dt*p_nu*grad(u)[0]*v*u*dx - u0*v*dx

T = 0.1
t = dt

u_over_time=[]

while t <= T:
    ud = Expression('2*x[0]/(2*t+1)', t=t, degree=2)
    bc = DirichletBC(V, ud, boundary)
    solve(F == 0, u, bc)
    t += dt
    u0.assign(u)
    u_evaluated = []
    for val in mesh.coordinates():
        u_evaluated.append(u(val))
    u_over_time.append(np.asarray(u_evaluated))


exact_solution = lambda x,t: 2*x/(2*t+1)

xvals = np.asarray(mesh.coordinates())
tvals = np.arange(dt, T, 10*dt)
xv, tv = np.meshgrid(xvals, tvals)

exact_solution_eval = exact_solution(xv, tv).T
numerical_solution = np.asarray(u_over_time).T

error = np.sum(np.abs(exact_solution_eval - numerical_solution[:,::10])**2, axis=0)