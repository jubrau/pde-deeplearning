from example_functions import initial_func3, initial_func1, initial_func2
import numpy as np
from periodic_data_utils import dx_derivative
import matplotlib.pyplot as plt
import json
import pandas as pd


def rational_root(x, numerator, denominator):
    return np.copysign(np.power(np.abs(x), 1.0 / denominator), x) ** numerator


def even_out(good_values):
    good_values_df = pd.DataFrame(good_values)

    def get_class_by_exponent(exponent):
        if exponent < 1:
            return "cl0"
        elif exponent == 1:
            return "cl1"
        elif exponent > 1:
            return "cl2"

    good_values_df['class'] = good_values_df['exponent'].apply(get_class_by_exponent)
    grouped_df = good_values_df.groupby('class')
    min_num = grouped_df.size().min()
    good_values = pd.DataFrame(grouped_df.apply(lambda x: x.sample(min_num)).reset_index(drop=True)).drop(
        columns='class').to_dict(orient='list')
    return good_values


good_values3 = {'a': [],
                'b': [],
                'c': [],
                'bt': [],
                'exponent': []}

past_exponents = []
for denominator in range(3, 10, 2):
    for numerator in range(1, 2 * denominator + 1, denominator // 3):
        exponent = float(numerator) / float(denominator)
        if exponent in past_exponents:
            continue
        past_exponents.append(exponent)
        for b in [1, 2]:
            for a in np.arange(2, 20, 2):
                for c in np.arange(a, 20, 2):
                    u0 = initial_func3(a, b, c, numpy=True)(np.linspace(0, 1, 1000))
                    derivatives = dx_derivative(u0[np.newaxis, :], 1. / 1000)
                    # subtract -1
                    new_numerator = numerator - denominator
                    values = exponent * rational_root(u0, new_numerator, denominator) * derivatives
                    bt = - 1. / np.min(values[:, 1:-1])
                    print(bt)
                    if bt > 0.20:
                        good_values3['a'].append(float(a))
                        good_values3['b'].append(float(b))
                        good_values3['c'].append(float(c))
                        good_values3['bt'].append(bt)
                        good_values3['exponent'].append(exponent)
                    # plt.plot(u0, label=round(bt, 2))
                    # plt.legend()
                # plt.show(block=True)
                # plt.clf()

good_values3 = even_out(good_values3)
with open('data/parameters3_exponent.json', 'w') as f:
    json.dump(good_values3, f)

good_values1 = {'a': [],
                'b': [],
                'bt': [],
                'exponent': []}

past_exponents = []
for denominator in range(3, 10):
    for numerator in range(1, 2 * denominator + 1):
        exponent = float(numerator) / float(denominator)
        if exponent in past_exponents:
            continue
        past_exponents.append(exponent)
        for b in np.arange(1, 5, 0.5):
            for a in np.arange(b + 0.5, 10, 0.5):
                for c in np.arange(a, 20, 2):
                    u0 = initial_func1(a, b, numpy=True)(np.linspace(0, 1, 1000))
                    derivatives = dx_derivative(u0[np.newaxis, :], 1. / 1000)
                    # subtract -1
                    new_numerator = numerator - denominator
                    values = exponent * rational_root(u0, new_numerator, denominator) * derivatives
                    bt = - 1. / np.min(values[:, 1:-1])
                    print(bt)
                    if bt > 0.20:
                        good_values1['a'].append(float(a))
                        good_values1['b'].append(float(b))
                        good_values1['bt'].append(bt)
                        good_values1['exponent'].append(exponent)
                    # plt.plot(u0, label=round(bt, 2))
                    # plt.legend()
                # plt.show(block=True)
                # plt.clf()

good_values1 = even_out(good_values1)
with open('data/parameters1_exponent.json', 'w') as f:
    json.dump(good_values1, f)

good_values2 = {'a': [],
                'b': [],
                'bt': [],
                'exponent': []}

past_exponents = []
for denominator in range(3, 10, 2):
    for numerator in range(1, 2 * denominator + 1, denominator // 3):
        exponent = float(numerator) / float(denominator)
        if exponent in past_exponents:
            continue
        past_exponents.append(exponent)
        for b in [1, 2]:
            if b == 1:
                a_range = np.arange(-1, 0, 0.05)
            else:
                a_range = np.arange(0.02, 0.2, 0.02)
            for a in a_range:
                u0 = initial_func2(a, b, numpy=True)(np.linspace(0, 1, 1000))
                derivatives = dx_derivative(u0[np.newaxis, :], 1. / 1000)
                exponent = float(numerator) / float(denominator)
                # subtract -1
                new_numerator = numerator - denominator
                values = exponent * rational_root(u0, new_numerator, denominator) * derivatives
                bt = - 1. / np.min(values[:, 1:-1])
                print(bt)
                if bt > 0.20:
                    good_values2['a'].append(float(a))
                    good_values2['b'].append(float(b))
                    good_values2['bt'].append(bt)
                    good_values2['exponent'].append(exponent)
                # plt.plot(u0, label=round(bt, 2))
                # plt.legend()
            # plt.show(block=True)
            # plt.clf()

good_values2 = even_out(good_values2)
with open('data/parameters2_exponent.json', 'w') as f:
    json.dump(good_values2, f)
