import numpy as np
from tensorflow.python.keras.utils import Sequence
import pandas as pd
import os
from periodic_data_utils import dx_derivative, dt_derivative
import json

class DataGenerator(Sequence):
    def __init__(self, dir_name, parameters, file_sequence_numbers=None, batch_size=None, shuffle=True,
                 fn_template=None, t_stride=1):
        """
        Initialize generator.
        :param dir_name: Directory name.
        :param parameters: List of parameters which to extract from loaded data, parameters grouped in a list are
                concatenated (supported: dt_u, dx_u, x_values, t_values, and scalar parameters)
        :param file_sequence_numbers: file indices to use from directory, if None use number of elements in directory
        :param batch_size: batch size
        :param shuffle: boolean, if true shuffle order of files and content of files
        :param fn_template: filename template according to which file names are generated, default data_{:04d}.json
        :param t_stride: stride for selecting t_values, for example only every second timestamp is used
        """
        self.t_stride = t_stride
        self.parameters = parameters
        self.fn_template = fn_template
        self.shuffle = shuffle
        self.file_sequence_numbers = file_sequence_numbers
        self.dir_name = dir_name
        self.file_index = 0
        if self.fn_template is None:
            self.fn_template = "data_{:04d}.json"
        if self.file_sequence_numbers is None:
            self.file_sequence_numbers = np.arange(len(os.listdir(self.dir_name)))
        self.file_number = 0
        self.current_file = None
        # load arbitrary file and deduce number of t / x values
        self.__update_internal_file(0)
        self.num_x_vals = len(self.data['solution'][0])
        self.batch_size = batch_size or self.num_x_vals
        self.location_indices = np.arange(self.num_x_vals)
        self.num_batch_per_file = int(self.num_x_vals // batch_size)
        self.num_total_t_vals = len(self.data['solution'])
        # to calculate how often stride fits in, we have to use ceil division
        self.num_used_t_vals = self.num_total_t_vals // t_stride + 1
        self.x_values, self.dx = np.linspace(0, 1, self.num_x_vals, endpoint=True, retstep=True)
        self.x_values = np.expand_dims(self.x_values, -1)
        self.T = self.data['T'] or 0.2
        self.t_values, self.dt = np.linspace(0, self.T, self.num_total_t_vals, endpoint=False, retstep=True)
        self.t_values = self.t_values[::self.t_stride]
        self.t_values = np.expand_dims(np.expand_dims(self.t_values, -1), 0)

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return self.num_batch_per_file * len(self.file_sequence_numbers)

    def __getitem__(self, item):
        """Generate batch of data with number of the batch specified by item"""
        # calculate in which batch in the file we are
        batch_number = item % self.num_batch_per_file
        # if we are at the beginning of a new batch, load next file, starting with the first file
        # (file_number initialized with 0)
        if batch_number == 0:
            self.__update_internal_file(self.file_number)
            self.add_derivatives(self.data)
            self.file_number += 1
        start = batch_number * self.batch_size
        end = start + self.batch_size
        input_data = self.__data_generation(self.data, self.location_indices[start:end])
        # print(item)
        return input_data, None

    def load_data_from_file(self, file_number):
        # build file name
        current_file = self.fn_template.format(self.file_sequence_numbers[file_number])
        # read file data
        with open(os.path.join(self.dir_name, current_file)) as f:
            data = json.load(f)
            # check how data was stored
        if isinstance(data, list):
            if len(data) == 1:
                data = data[0]
            else:
                raise ValueError("Loaded datatype not understood: loaded list of length {}".format(len(self.data)))
        elif not isinstance(data, dict):
            raise ValueError("Loaded data not understood, neither dict nor list: {}".format(self.data))
        return data

    def __update_internal_file(self, file_number):
        self.data = self.load_data_from_file(file_number)

    def add_derivatives(self, data):
        # generate derivatives if necessary, final shape (num_x_values, num_used_t_vals, 1)
        if 'dx_u' in self.parameters:
            dx_u = np.expand_dims(dx_derivative(data['solution'][::self.t_stride], self.dx).T, -1)
            data['dx_u'] = dx_u
        if 'dt_u' in self.parameters:
            dt_u = np.expand_dims(dt_derivative(data['solution'], self.dt).T, -1)
            dt_u = dt_u[:, ::self.t_stride, :]
            data['dt_u'] = dt_u

    def on_epoch_end(self):
        """Shuffle indices if shuffling is enabled and reset file number"""
        if self.shuffle:
            np.random.shuffle(self.file_sequence_numbers)
            np.random.shuffle(self.location_indices)
        self.file_number = 0

    def __data_generation(self, data, indices):
        """
        Generate data by extracting given parameters from dataframe data or generating it from scratch.
        :param data: pandas dataframe
        :param indices: indices which should be used in this batch
        :return: list of numpy arrays
        """
        generated_data = []
        for parameter in self.parameters:
            if isinstance(parameter, (list, tuple)):
                temp_data = []
                for p in parameter:
                    temp_data.append(self.to_batch(data[p], indices))
                generated_data.append(np.concatenate(temp_data, axis=-1))
            else:
                if parameter in data.keys():
                    generated_data.append(self.to_batch(data[parameter], indices))
                elif parameter is "x_values":
                    generated_data.append(self.x_values[indices])
                elif parameter is "t_values":
                    generated_data.append(np.repeat(self.t_values, self.batch_size, axis=0))
                else:
                    raise ValueError(
                        f'Requested parameter {parameter} not found in keys of dictionary with keys {data.keys()}')
        return generated_data

    def to_batch(self, data, indices=None):
        """
        Broadcast to batch shape and extract indices.
        :param indices: Indices at which to extract data.
        :param data: data, either of shape (batch_dim, n1, n2,...) or 0 dimensional (scalar)
        :return: data either broadcasted along batch, or batch elements are extracted according to indices.
        """
        data = np.asarray(data)
        bcast_data = data
        # data is scalar: broadcast
        if np.ndim(data) == 0:
            bcast_data = np.ones((self.batch_size, 1)) * data
        # check that first dimension is batch dimension
        elif np.ndim(data) >= 1 and data.shape[0] < self.batch_size:
            raise ValueError(
                "First dimension seems not to be batch dim or too small, with data of shape {}".format(data.shape))
        elif indices is None:
            # check that indices are specified
            raise AttributeError(
                "Parameter 'indices' has to be specified for data of shape {} (non-scalar data).".format(data.shape))
        else:
            bcast_data = bcast_data[indices]
        return bcast_data

    def yield_whole_file(self, file_number):
        data = self.load_data_from_file(file_number)
        self.add_derivatives(data)
        return self.__data_generation(data, range(len(self.x_values)))


if __name__ == '__main__':
    data_gen = DataGenerator('data/single_files/single_files_2/',
                             parameters=[['a', 'b'], 't_values', 'dt_u', 'dx_u', 'x_values'], t_stride=3000 // 200)
    batch_data = data_gen.__getitem__(0)
