import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from periodic_data_utils import dx_derivative

dataset1 = np.load('data/parameters1.npy').item()
dataset2 = np.load('data/parameters2.npy').item()
dataset3 = np.load('data/parameters3.npy').item()
dataset3_old = np.load('data/parameters3_old.npy').item()
dataset3_precise = np.load('data/parameters3_precise.npy').item()

dataset = dataset3_precise

dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]

dataset['characteristics_derivative'] = []
for value in dataset['characteristics']:
    dataset['characteristics_derivative'].append(dx_derivative(value, dx)[:, :-1])

for key, value in dataset.items():
    print(str(key) + ": " + str(len(value)) + " entries of shape " + str(np.asarray(value[0]).shape))


def plot_something(i, key='sol', t_slice=None, x_slice=None, inverse=False, block=False):
    x_values = dataset['x_values'][i]
    y_values = np.asarray(dataset[key][i]).T
    if x_slice is not None:
        x_values = x_values[x_slice]
    if t_slice is not None:
        y_values = y_values[:, t_slice]
    if x_slice is not None:
        y_values = y_values[x_slice]
    if not inverse:
        plt.plot(x_values, y_values)
    else:
        plt.plot(y_values, x_values)
    plt.show(block=block)


min_derivative_values = np.min(np.asarray(dataset['characteristics_derivative'])[:, -1, :], axis=-1)
index = np.argmin(min_derivative_values)
print("Amount of non-monotonic characteristics:" + str(np.count_nonzero(min_derivative_values < 0)))
negative_indices = np.where(min_derivative_values < 0)[0]
print([dataset['bt'][negative_index] for negative_index in negative_indices])
plot_something(index, 'characteristics_inverse', t_slice=-1, x_slice=slice(0, 300))
plot_something(index, 'characteristics', t_slice=-1, x_slice=slice(0, 300), inverse=True)
# plt.figure()
# for index in negative_indices:
#     print("Min derivative value: {}  Breaking time: {}".format(min_derivative_values[index], dataset['bt'][index]))
#     plot_something(index, 'sol', t_slice=slice(None, None, 4), block=True)
#     plt.clf()
