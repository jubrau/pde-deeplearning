import numpy as np
import pandas as pd

# t_values =np.linspace(0, 0.2, 200, endpoint=False)
# x_values = np.linspace(0, 1, 1001, endpoint=True)

dataset1_1 = pd.DataFrame(np.load('data/parameters1_precise.npy').item())
dataset2_1 = pd.DataFrame(np.load('data/parameters2_precise.npy').item())
dataset3_1 = pd.DataFrame(np.load('data/parameters3_precise.npy').item()).drop('c', 1)
dataset1_0 = pd.DataFrame(np.load('data/transport_equation_1.npy').item())
dataset2_0 = pd.DataFrame(np.load('data/transport_equation_2.npy').item())
dataset3_0 = pd.DataFrame(np.load('data/transport_equation_3.npy').item()).drop('c', 1)
dataset_1 = pd.concat([dataset1_1, dataset3_1, dataset2_1])
dataset_1.insert(0, "exponent", 1)
dataset_0 = pd.concat([dataset1_0, dataset3_0, dataset2_0])
dataset_0.insert(0, "exponent", 0)
dataset = pd.concat([dataset_1, dataset_0])
dataset = dataset[['solution', 'characteristics_inverse', 'characteristics', 'exponent']]
dataset = dataset.reset_index()
dataset.to_hdf('data/dataset_complete.h5', key='df')
