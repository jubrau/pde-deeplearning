import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow.python import keras
from tensorflow.python.keras import Model
from tensorflow.python.keras.layers import Dense, Concatenate, Input, Lambda

tf.compat.v1.disable_eager_execution()


class VFieldRNNCell(keras.layers.Layer):

    @property
    def output_size(self):
        return [1, 1, 1]

    def __init__(self, parameter_units, input_units, dt, activation='relu', **kwargs):
        self.parameter_units = parameter_units
        self.input_units = input_units
        self.dt = dt
        self.activation = activation
        super().__init__(**kwargs)

    @property
    def state_size(self):
        return [1, 1]

    def build(self, input_shape):
        self.parameter_transform = Dense(self.parameter_units, activation=self.activation, use_bias=True,
                                         name='transformed_parameter')

        self.input_transform = Dense(self.input_units, activation=self.activation, use_bias=True,
                                     name='transformed_state')
        self.final_transform = Dense(1, activation=None, use_bias=True)
        self.built = True

    # input_at_t: parameters (including time)
    # state_at_t: phi(t), grad_phi(t)
    # outputs: v(phi(t)) (=dt_phi(t))
    # state_at_(t+1): phi(t+1), grad_phi(t+1)
    def call(self, inputs, **kwargs):
        input_at_t = inputs
        state_at_t = kwargs['states']
        phi = state_at_t[0]
        grad_phi = state_at_t[1]
        v_phi = self.apply_v_field(phi, input_at_t)
        grad_v_phi = tf.gradients(v_phi, phi)[0]
        phi_new = phi + self.dt * v_phi
        grad_phi_new = grad_phi + self.dt * grad_v_phi * grad_phi
        return v_phi, phi_new, grad_phi_new

    def apply_v_field(self, inputs, parameter):
        v_phi_intermediate = Concatenate()([self.input_transform(inputs), self.parameter_transform(parameter)])
        v_phi = self.final_transform(v_phi_intermediate)
        return v_phi


class PdeRNN:

    def __init__(self, num_t_vals, parameter_units, input_units, dt, dx, num_parameters=1,
                 num_parameters_depending_on_t=0):
        self.num_parameters_depending_on_t = num_parameters_depending_on_t
        self.num_parameters = num_parameters
        self.dx = dx
        self.dt = dt
        self.input_units = input_units
        self.parameter_units = parameter_units
        self.num_t_vals = num_t_vals

    def build(self, **kwargs):
        input_dx_u = Input(shape=(self.num_t_vals, 1), dtype='float', name='dx_u')
        input_dt_u = Input(shape=(self.num_t_vals, 1), dtype='float', name='dt_u')
        if self.num_parameters_depending_on_t > 0:
            input_parameters_t = Input(dtype='float', name='parameters_depending_on_t',
                                       shape=(self.num_t_vals, self.num_parameters_depending_on_t))
        if self.num_parameters > 0:
            input_parameters = Input(dtype='float', name='parameters_independent_of_t', shape=(self.num_parameters,))
        input_time = Input(dtype='float', name='time', shape=(self.num_t_vals, 1))
        input_x = Input(dtype='float', name='input_x', shape=(1,))

        cell = VFieldRNNCell(self.parameter_units, self.input_units, self.dt, name='v_field', **kwargs)

        # initial values: phi = id
        phi = Lambda(lambda x: x, name='initial_phi')(input_x)
        grad_phi = keras.backend.ones_like(phi)
        loss = 0
        grad_loss = 0
        dt_loss = 0
        phi_list = [phi]
        v_field_list = []

        for i in range(self.num_t_vals - 1):
            # use Lambda to preserve keras_history use a lambda layer
            time_at_i = Lambda(lambda x: x[:, i], name='time_at_{}'.format(i))(input_time)
            if self.num_parameters_depending_on_t > 0:
                parameters_at_i = Lambda(lambda x: x[:, i], name='parameters_at_{}_v1'.format(i))(input_parameters_t)
                if self.num_parameters > 0:
                    parameters_at_i = Concatenate(name=f'parameters_at_{i}')([parameters_at_i, input_parameters])
            elif self.num_parameters > 0:
                parameters_at_i = input_parameters
            if self.num_parameters > 0 or self.num_parameters_depending_on_t > 0:
                input_parameters_i = Concatenate(name='input_parameters_at_{}'.format(i))([time_at_i, parameters_at_i])
            else:
                input_parameters_i = time_at_i
            # get values phi_(i+1), grad_phi_(i+1)
            v_phi_at_i, phi_new, grad_phi_new = cell(input_parameters_i, states=[phi, grad_phi])
            phi_list.append(phi_new)
            output_v_field = cell.apply_v_field(input_x, input_parameters_i)
            v_field_list.append(output_v_field)
            # don't use first value because dt_u is inaccurate
            if i > 0:
                loss += tf.reduce_sum(
                    (input_dt_u[:, i] - v_phi_at_i * input_dx_u[:, i] / grad_phi) ** 2 * grad_phi) * self.dt * self.dx
            grad_loss += tf.reduce_sum(grad_phi ** 2 * self.dt * self.dx)
            dt_loss += tf.reduce_sum(v_phi_at_i ** 2) * self.dt * self.dx
            # set new values for next iteration
            phi = phi_new
            grad_phi = grad_phi_new
        # model.add_loss(0.05 * dt_loss)
        if self.num_parameters_depending_on_t > 0:
            if self.num_parameters > 0:
                self.model = Model([input_parameters, input_parameters_t, input_time, input_x], phi_list)
                self.model_w_loss = Model(
                    [input_parameters, input_parameters_t, input_time, input_dt_u, input_dx_u, input_x], phi_list)
                self.v_field_model = Model([input_parameters, input_parameters_t, input_time, input_x], v_field_list)
            else:
                self.model = Model([input_parameters_t, input_time, input_x], phi_list)
                self.model_w_loss = Model([input_parameters_t, input_time, input_dt_u, input_dx_u, input_x], phi_list)
                self.v_field_model = Model([input_parameters_t, input_time, input_x], v_field_list)
        elif self.num_parameters > 0:
            self.model = Model([input_parameters, input_time, input_x], phi_list)
            self.model_w_loss = Model([input_parameters, input_time, input_dt_u, input_dx_u, input_x], phi_list)
            self.v_field_model = Model([input_parameters, input_time, input_x], v_field_list)
        else:
            self.model = Model([input_time, input_x], phi_list)
            self.model_w_loss = Model([input_time, input_dt_u, input_dx_u, input_x], phi_list)
            self.v_field_model = Model([input_time, input_x], v_field_list)
        self.model_w_loss.add_loss(loss)
        return self.model_w_loss, self.model

    # to predict, where x is transported (from) over time depending on the parameter given
    def predict_nn_function(self, parameter_values, x, t_values):
        # parameter_values: (batch_size, 1)
        # t_vals: (batch_size, num_t_vals, 1)
        t_values = np.squeeze(t_values)[np.newaxis, :, np.newaxis]
        t_vals = np.tile(t_values, (len(parameter_values), 1, 1))
        x_vals = np.ones_like(parameter_values) * x
        return self.model.predict([parameter_values, t_vals, x_vals])

    def plot_nn_function(self, parameter_values, x, t_values):
        out = self.predict_nn_function(parameter_values, x, t_values)
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
        plt.ylabel(r'$\phi^{-1}({x})$')
        plt.xlabel(r'$u_0(x)$')
        plt.title(r'x = {}'.format(x))
        plt.plot(parameter_values, np.asarray(out)[:, :, 0].T)
        plt.legend(list(map(lambda t: 't=' + str(np.round(t, 2)), t_values)))

    # evaluate on ith data
    def evaluate(self, i, dataset):
        t_values = dataset['t_values'][i]
        x_values = dataset['x_values'][i]
        if self.num_parameters_depending_on_t > 0:
            if self.num_parameters > 0:
                phi = self.model.predict(
                    [dataset['parameters_independent_of_t'][i], dataset['parameters_depending_on_t'][i], t_values,
                     x_values])
            else:
                phi = self.model.predict([dataset['parameters_depending_on_t'][i], t_values, x_values])
        elif self.num_parameters > 0:
            phi = self.model.predict([dataset['parameters_independent_of_t'][i], t_values, x_values])
        else:
            phi = self.model.predict([t_values, x_values])
        t_vals = t_values[0, :-1, 0]
        x_vals = np.round(x_values[::50, 0], 2)
        plt.figure()
        plt.title('Learned coordinate transform')
        plt.plot(t_vals, np.asarray(phi)[:, :-1:50, 0], )
        plt.xticks(t_vals[::len(t_vals) // 10])
        plt.xlabel('time')
        plt.ylabel('y')
        plt.xlim(left=0, right=t_vals[-1])
        plt.ylim(bottom=-0.1, top=1)
        plt.yticks(x_vals)
        plt.tick_params(labelleft=True, labelright=True, left=True, right=True)
        plt.legend(list(map(lambda x: 'x={}'.format(x), x_vals)))
        plt.figure()
        plt.title('True inverse characteristics')
        plt.xticks(t_vals[::myround(len(t_vals) // 10)])
        plt.xlim(left=0, right=t_vals[-1])
        plt.xlabel('time')
        plt.ylabel('y')
        plt.ylim(bottom=-0.1, top=1)
        plt.tick_params(labelleft=True, labelright=True, left=True, right=True)
        plt.yticks(x_vals)
        plt.plot(t_vals, dataset['characteristics_inverse'][i][:-1, ::50])
        plt.legend(list(map(lambda x: 'x={}'.format(x), x_vals)))

    def evaluate_v_field(self, i, dataset):
        t_values = dataset['t_values'][i]
        x_values = dataset['x_values'][i]
        if self.num_parameters_depending_on_t > 0:
            if self.num_parameters > 0:
                v_field = self.v_field_model.predict(
                    [dataset['parameters_independent_of_t'][i], dataset['parameters_depending_on_t'][i], t_values,
                     x_values])
            else:
                v_field = self.v_field_model.predict([dataset['parameters_depending_on_t'][i], t_values, x_values])
        elif self.num_parameters > 0:
            v_field = self.v_field_model.predict([dataset['parameters_independent_of_t'][i], t_values, x_values])
        else:
            v_field = self.v_field_model.predict([t_values, x_values])
        return v_field

def plot_train_function(x, dataset, t_range=None):
    t_values = dataset['t_values'][0]
    if t_range is not None:
        t_values = t_values[t_range]
    idx = np.argwhere(np.abs(dataset['x_values'] - x) <= 10 * np.finfo(np.float64).resolution)[:, 0]
    values = dataset['initial_functions'][idx]
    characteristics_inv = np.concatenate(
        list(map(lambda x: np.concatenate((x, np.ones((x.shape[0], 1))), axis=-1), dataset['characteristics_inv'])),
        axis=1).T
    characteristics_inv_values = characteristics_inv[idx, :]
    sorting = np.argsort(values)
    values = values[sorting]
    characteristics_inv_values = characteristics_inv_values[sorting]
    plt.plot(values, characteristics_inv_values[:, :-1])
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.xlabel(r'Available $u_0(x)$ values in training set')
    plt.gcf().legend(list(map(lambda x: 't=' + str(np.round(x[0], 2)), t_values)))
    plt.ylabel(r'$\phi^{-1}({x})$')
    plt.title(r'x = {}'.format(x))


def myround(x, base=5):
    return base * round(x / base)
