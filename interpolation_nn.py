import tensorflow as tf
from tensorflow.python.keras.layers import Dense, Input, Concatenate, Dropout
from tensorflow.python.keras import Model
import numpy as np
from periodic_data_utils import periodic_coordinates, extend_function, generate_solutions_to_transport_equation
from sklearn.model_selection import train_test_split

input_x = Input(shape=(1,))
input_y = Input(shape=(1,))
hidden_layer = Dropout(0.1)(Dense(100, activation='relu')(input_y))
y = Dense(1)(Concatenate()([hidden_layer, input_x]))

model = Model([input_x, input_y], y)
model.compile(optimizer='Adam', loss='mse')


def f(x, a=2, b=2):
    return np.sin(a * np.pi * x) * (np.cos(2 * np.pi * x) - 1) ** b
    # return (np.cos(2 * np.pi * x) - 1)**2


def generate_data(x_vals, t_vals, velocity=1, wrap=1, a=2, b=2):
    xv, tv = np.meshgrid(x_vals, t_vals)
    coordinates = periodic_coordinates(xv - velocity * tv, wrap)
    f_extended = extend_function(f, 0, 1)
    data = f_extended(coordinates, a, b)
    return data


dx = 0.01
dt = 0.001
end_x = 3
x_vals = np.arange(0, end_x, dx)
t_vals = np.arange(0, 0.01, dt)
num_x_vals = len(x_vals)
num_t_vals = len(t_vals)

dataset = generate_solutions_to_transport_equation(f, x_vals, t_vals, dt, dx, wrap=3, a=[2], b=[2],
                                                   velocity=np.arange(1, 10, 0.2))

x_vals_total = np.tile(x_vals, (len(dataset['u']) * num_t_vals, 1))
u = np.concatenate(dataset['u'])
*split_data, = train_test_split(x_vals_total, u, test_size=0.33)
split_data = list(map(np.ndarray.flatten, split_data))
x_train, x_test, u_train, u_test = *split_data,
model.fit([x_train, u_train], u_train, validation_data=([x_test, u_test], u_test), batch_size=20, epochs=10,
          validation_split=0.3)

