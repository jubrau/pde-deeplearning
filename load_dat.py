# import ijson
# import json
import pandas as pd
import timeit

# def load_json(filename):
#     with open(filename, 'r') as fd:
#         parser = ijson.parse(fd)
#         ret = {'builders': {}}
#         for a in parser:
#             print("hllo")
#         objects = ijson.items(f, 'meta.view.columns.item')
#         return ret
#
# load_json('data/dataset_complete.json')
#
# with open('data/dataset_complete.json') as json_file:
#     data = json.load(json_file)
# df = pd.read_hdf('data/dataset_complete.h5', key="df", chunksize=100)
store = pd.HDFStore('data/dataset_complete.h5', mode='r')
storer = store.get_storer('df')
# nrows = storer.shape[0]
chunksize = 1


@profile
def read_pandas_hdf():
    return pd.read_hdf('data/dataset_complete.h5', mode='r')


i = 1


@profile
def read_chunk():
    for i in range(3):
        data = storer.read(i * chunksize, (i + 1) * chunksize)
    return data


# for i in range(nrows//chunksize + 1):
#     data = read_chunk(storer, i, chunksize)
# for chunk in pd.read_hdf('data/dataset_complete.h5', key="df", chunksize=100):
#     print(chunk.shape)

# print(timeit.timeit(read_pandas_hdf, number=1))
print(timeit.timeit(read_chunk, number=1))
