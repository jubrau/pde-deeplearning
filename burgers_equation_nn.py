import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.python.keras import optimizers

from nn_architectures import PdeRNN
from periodic_data_utils import dx_derivative, dt_derivative

tf.compat.v1.disable_eager_execution()

if len(sys.argv) > 1:
    fit = sys.argv[1] == '-fit'
else:
    fit = False

dataset = np.load('data/parameters1.npy').item()
dataset2 = np.load('data/parameters2.npy').item()
dataset3 = np.load('data/parameters3_old.npy').item()

num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
x_vals = dataset['x_values'][0]
t_vals = np.asarray(dataset['t_values'][0])
dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]
dt = dataset['t_values'][0][1] - dataset['t_values'][0][0]

dt_u = [np.expand_dims(dt_derivative(u, dt).T, -1) for u in dataset['sol']]
dx_u = [np.expand_dims(dx_derivative(u, dx).T, -1) for u in dataset['sol']]
t_values = [np.broadcast_to(np.asarray(t_value)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1))
            for t_value in dataset['t_values']]
x_values = dataset['x_values'][0]
parameters = [np.concatenate([np.broadcast_to(a, (num_x_vals, 1)), np.broadcast_to(b, (num_x_vals, 1))], axis=-1)
              for a, b in zip(dataset['a'], dataset['b'])]

dataset_new = {'dt_u': dt_u,
               'dx_u': dx_u,
               't_values': t_values,
               'parameters': parameters,
               'characteristics_inv': dataset['characteristics_inv'],
               'x_values': dataset['x_values']}

# convert to dataframe for sklearn support
dataset_df = pd.DataFrame(dataset_new)
# split rows into two
train, test = train_test_split(dataset_df, test_size=0.1, random_state=10, shuffle=True)
# convert back to dict
train = train.to_dict(orient='list')
test = test.to_dict(orient='list')

for key, value in train.items():
    if key is not 'characteristics_inv':
        train[key] = np.concatenate(value, axis=0)

for key, value in test.items():
    if key is not 'characteristics_inv':
        test[key] = np.concatenate(value, axis=0)

pde_rnn = PdeRNN(num_t_vals, parameter_units=200, input_units=60, dt=dt, dx=dx, num_parameters=2)
pde_rnn.build(activation='relu')
model = pde_rnn.model_w_loss

if fit:
    model.compile(optimizer=optimizers.Adam(lr=0.001))
    history = model.fit([train['parameters'],
                         train['t_values'],
                         train['dt_u'],
                         train['dx_u'],
                         train['x_values']],
                        validation_data=([test['parameters'],
                                          test['t_values'],
                                          test['dt_u'],
                                          test['dx_u'],
                                          test['x_values']], None),
                        batch_size=100,
                        epochs=500,
                        shuffle=True,
                        verbose=2)
    model.save_weights('results/withab/weights.h5')
    np.save('results/withab/history.npy', history.history)
else:
    model.load_weights('results/withab/weights.h5')
    pde_rnn.evaluate(30, dataset_new)
    history = np.load('results/withab/history.npy').item()
    plt.figure()
    plt.plot(np.arange(len(history['loss']))[::20], history['loss'][::20], label='loss')
    plt.plot(np.arange(len(history['val_loss']))[::20], history['val_loss'][::20], label='val_loss')
    plt.legend()
    plt.show()
