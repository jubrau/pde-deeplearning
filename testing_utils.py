from periodic_data_utils import generate_derivatives
import numpy as np


# t: rows
# x: columns
def test_hyperbolic_wave(data, dx, dt, nonlinear_func=None, x_mode='forward', t_mode='forward'):
    data_t, data_x = generate_derivatives(data, dt=dt, dx=dx, t_mode=t_mode, x_mode=x_mode)
    if nonlinear_func is None:
        diff = data_t + data * data_x
    else:
        diff = data_t + nonlinear_func(data) * data_x
    return diff


def test_transport(data, dx, dt, velocity, x_mode='forward', t_mode='forward'):
    return test_hyperbolic_wave(data, dx, dt, lambda x: velocity, x_mode, t_mode)
