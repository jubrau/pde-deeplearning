import numpy as np


# inspired by scipy.interpolate.RegularGridInterpolator._find_indices
def find_closest_indices(grid_points, vals, dx):
    # find relevant edges between which xi are situated
    i = np.searchsorted(grid_points, vals, side='left')
    i[i < 0] = 0
    i[i > len(grid_points) - 1] = len(grid_points) - 1
    distances = np.abs(vals - grid_points[i])
    i = np.where(distances > dx / 2, i - 1, i)
    i[i < 0] = 0
    i[i > len(grid_points) - 1] = len(grid_points) - 1
    out_of_bounds = np.logical_or(vals < grid_points[0], vals > grid_points[-1])
    return i, out_of_bounds, distances


# "hat function" in center with radius dx
def p1_function(center, dx):
    def basis_function(x):
        eps = np.finfo(x.dtype).resolution
        x = np.asarray(x)
        in_cell = np.abs(x - center) <= dx + eps
        left_value = (x - (center - dx)) / dx
        right_value = (center + dx - x) / dx
        result = np.where(in_cell,
                          np.where(x <= center + eps, left_value, right_value),  # different cases inside
                          np.zeros(x.shape))  # value outside of cell
        return result

    return basis_function

