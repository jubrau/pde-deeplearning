import numpy as np
import matplotlib.pyplot as plt

t0 = 0
num_elements = 600
p_lbd = 1.3
p_nu = 2
dt = 0.001

u0 = lambda x: p_lbd + np.sin(np.pi * x)
char_curve = lambda t, x0: x0 + p_nu*u0(x0) * t

# manually calculated breaking time
breaking_time = 1/(np.pi*p_nu)
discontinuity_point = -1 + breaking_time*u0(-1)*p_nu

x0_vals = np.linspace(-2, 1.5, 1000)
t_vals = np.arange(0, 0.5, dt)
char_curves = []
for x_val in x0_vals:
    char_curves.append(char_curve(t_vals, x_val))

plt.plot(t_vals, np.asarray(char_curves[::10]).T)
plt.axvline(x=breaking_time)
plt.ylim(bottom=-1.1, top=1.1)
plt.xlim(left=0, right=0.5)
plt.xticks([breaking_time, 0.4], ['breaking time', '0.4'], )
plt.show()

x_vals = np.linspace(-1, 1, num_elements)
u = []
char_curves = np.asarray(char_curves)
for t_idx, t_val in enumerate(t_vals):
    if t_val > breaking_time + 0*dt:
        break
    y_vals=[]
    for x_val in x_vals:
        y_val = u0(x0_vals[np.argmin(np.abs(char_curves[:,t_idx]-x_val))])
        y_vals.append(y_val)
    u.append(y_vals)
u = np.asarray(u)
plt.plot(x_vals, u[::10].T)
plt.plot(x_vals, u[-1])
plt.axvline(x=discontinuity_point)
plt.show()

