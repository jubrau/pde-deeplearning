import numpy as np
from periodic_data_utils import dx_derivative, dt_derivative
import matplotlib.pyplot as plt
import pynverse

dataset = np.load('data/parameters1.npy').item()

num_t_vals = len(dataset['t_values'][0])
num_x_vals = len(dataset['x_values'][0])
x_vals = dataset['x_values'][0]
dx = dataset['x_values'][0][1] - dataset['x_values'][0][0]
dt = dataset['t_values'][0][1] - dataset['t_values'][0][0]

dt_u = [np.expand_dims(dt_derivative(np.asarray(u), dt).T, -1) for u in dataset['sol']]
dx_u = [np.expand_dims(dx_derivative(np.asarray(u), dx).T, -1) for u in dataset['sol']]
t_values = [np.broadcast_to(np.asarray(t_value)[np.newaxis, :, np.newaxis], shape=(num_x_vals, num_t_vals, 1)) for
            t_value in
            dataset['t_values']]
x_values = dataset['x_values'][0]
parameters = [
    np.concatenate([np.broadcast_to(a, shape=(num_x_vals, 1)), np.broadcast_to(b, shape=(num_x_vals, 1))], axis=-1) for
    a, b in zip(dataset['a'], dataset['b'])]
parameters_b = [np.broadcast_to(b, shape=(num_x_vals, 1)) for b in dataset['b']]

dataset_new = {'dt_u': dt_u, 'dx_u': dx_u, 't_values': t_values, 'parameters': parameters,
               'characteristics_inv': dataset['characteristics_inv']}


def loss(dt_u, dx_u, phi, dx, dt):
    dx_phi = np.expand_dims(dx_derivative(phi, dx, x_mode='central').T, -1)[1:-1, 1:-1]
    dt_phi = np.expand_dims(dt_derivative(phi, dx).T, -1)[1:-1, 1:-1]
    return (dt_u - dt_phi * dx_u / dx_phi) ** 2 * dx_phi * dx * dt


identity = np.broadcast_to(np.expand_dims(np.squeeze(x_values), 0), shape=(num_t_vals, num_x_vals))

loss_char = loss(dataset_new['dt_u'][0][1:-2, 1:-1], dataset_new['dx_u'][0][1:-2, 1:-1],
                 np.squeeze(dataset_new['characteristics_inv'][0]),
                 dx,
                 dt)
loss_char_val = np.sum(loss_char)
loss_identity = loss(dataset_new['dt_u'][0][1:-1, 1:-1], dataset_new['dx_u'][0][1:-1, 1:-1], identity,
                     dx,
                     dt)
loss_identity_val = np.sum(loss_identity)
plt.plot(loss_char[:, :, 0])
plt.figure()
plt.plot(np.asarray(dataset['sol'][0]).T[1:-1, 1:-1])
