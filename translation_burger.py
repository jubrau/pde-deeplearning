import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from periodic_data_utils import generate_derivatives


def generate_training_data(x_vals, t_vals, nu, eps=1e-8, period=2):
    u = []
    offset = min(x_vals) + 1
    for t in t_vals:
        left_jump = (t * nu - 1 / 2 + offset) % period
        right_jump = (t * nu + 1 / 2 + offset) % period
        if left_jump < right_jump:
            row = np.logical_and(left_jump - eps <= x_vals, x_vals < right_jump - eps).astype(np.float)
        else:
            row = np.logical_or(x_vals < right_jump - eps, x_vals >= left_jump - eps).astype(np.float)
        u.append(row)
    return np.asarray(u)


dx = 0.1
x_vals = np.arange(0, 2, dx)
num_elements = len(x_vals)
dt = 0.1
t_vals = np.arange(0, 2, dt)
num_t_vals = len(t_vals)
u = generate_training_data(x_vals, t_vals, nu=1)
dt_u, dx_u = generate_derivatives(u, dt, dx)

training_data = {'u': [],
                 'dt_u': [],
                 'dx_u': [],
                 'nu': []}

for nu in np.arange(0, 10, 1):
    u = generate_training_data(x_vals=x_vals, t_vals=t_vals, nu=nu)
    dt_u, dx_u = generate_derivatives(u, dt, dx)
    training_data['u'].append(u)
    training_data['dt_u'].append(dt_u)
    training_data['dx_u'].append(dx_u)
    training_data['nu'].append(np.array(nu, ndmin=1))


input_dx_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_elements, 1))
input_dt_u = tf.placeholder(dtype='float', shape=(num_t_vals, num_elements, 1))
input_x = tf.constant(value=x_vals, dtype='float', shape=(num_elements, 1))
var_c = tf.Variable(initial_value=0, trainable=True, name='c', dtype='float')
input_nu = tf.placeholder(dtype='float', shape=(1, 1))

phi_xt = input_x
phi_values = [phi_xt]
loss = 0

for i in range(1, num_t_vals):
    phi_xt = phi_xt + dt * var_c * input_nu
    phi_values.append(phi_xt)
    loss += tf.reduce_sum((input_dt_u[i] - var_c * input_nu * input_dx_u[i]) ** 2)

opt = tf.train.AdamOptimizer(learning_rate=0.1).minimize(loss)

with tf.Session() as sess:
    tf.global_variables_initializer().run()
    nb_epochs = 100
    c_vals = np.zeros((nb_epochs, 1))
    current_c = 0
    plt.ion()
    for i in range(nb_epochs):
        for j in range(len(training_data['u'])):
            feed_dict = {input_dx_u: np.expand_dims(training_data['dx_u'][j], -1),
                         input_dt_u: np.expand_dims(training_data['dt_u'][j], -1),
                         input_nu: np.expand_dims(training_data['nu'][j], -1)}
            current_loss, current_c, _ = sess.run([loss, var_c, opt], feed_dict=feed_dict)
            # print("Loss:" + str(current_loss))
            # print("C:" + str(current_c))
        c_vals[i] = current_c
        plt.clf()
        plt.plot(np.arange(nb_epochs), c_vals)
        print(current_c)

