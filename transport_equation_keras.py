import numpy as np
from periodic_data_utils import periodic_coordinates, extend_function, generate_solutions_to_transport_equation
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras.layers import Dense, Concatenate, Input
import pandas as pd
from sklearn.model_selection import train_test_split
from tensorflow.python.keras import Model
from tensorflow.python.keras import optimizers
from tensorflow.python import keras

tf.compat.v1.disable_eager_execution()

dx = 0.01
dt = 0.001
end_x = 3
x_vals = np.arange(0, end_x, dx)
t_vals = np.arange(0, 0.01, dt)
num_x_vals = len(x_vals)
num_t_vals = len(t_vals)


def f(x, a=2, b=2):
    return np.sin(a * np.pi * x) * (np.cos(2 * np.pi * x) - 1) ** b
    # return (np.cos(2 * np.pi * x) - 1)**2


# sanity check
def dx_f_fun(x, a=2, b=2):
    return a * np.pi * (np.cos(2 * np.pi * x) - 1) ** b - np.sin(a * np.pi * x) * b * (np.cos(2 * np.pi * x) - 1) ** (
            b - 1) * np.sin(2 * np.pi * x) * 2 * np.pi
    # return -4 * np.pi * np.sin(2 * np.pi * x) * (np.cos(2 * np.pi * x) - 1)


def generate_data(x_vals, t_vals, velocity=1, wrap=1, a=2, b=2):
    xv, tv = np.meshgrid(x_vals, t_vals)
    coordinates = periodic_coordinates(xv - velocity * tv, wrap)
    f_extended = extend_function(f, 0, 1)
    data = f_extended(coordinates, a, b)
    return data


def phi_reference(x_vals, t_vals, velocity):
    xv, tv = np.meshgrid(x_vals, t_vals)
    return periodic_coordinates(xv - velocity * tv, 3)


dataset = generate_solutions_to_transport_equation(f, x_vals, t_vals, dt, dx, wrap=3, a=[2], b=[2],
                                                   velocity=np.arange(1, 10, 0.2))

dataset_df = pd.DataFrame(dataset)

train, test = train_test_split(dataset_df, test_size=0.33, random_state=42, shuffle=True)

# sanity check
diff = []
xv, tv = np.meshgrid(x_vals, t_vals)
for i in range(6):
    dt_u = dataset['dt_u'][i][1:-1, :]
    dx_u = dataset['dx_u'][i][1:-1, :]
    u = dataset['u'][i]
    v = dataset['velocity'][i]
    a = dataset['a'][i]
    b = dataset['b'][i]
    coordinates = periodic_coordinates(xv - v * tv, 3)
    dx_f = extend_function(dx_f_fun, 0, 1)(coordinates, a, b)
    diff.append(np.abs(dt_u + v * dx_u))
    if np.max(np.average(diff)) > 0.1:
        print("WARNING: " + str(np.average(diff)))

# array of shape (num_velocities*num_xvals, num_t_vals, 1) from (num_velocities, num_xvals, num_tvals)
dx_u_test = np.stack(test['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
dt_u_test = np.stack(test['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
velocity_test = np.stack(test['velocity'].values).reshape((-1, 1))

u_train = np.stack(train['u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
dx_u_train = np.stack(train['dx_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
dt_u_train = np.stack(train['dt_u'].values).transpose([0, 2, 1]).reshape((-1, num_t_vals, 1))
velocity_train = np.stack(train['velocity'].values).reshape((-1, 1))


class VFieldRNNCell(keras.layers.Layer):

    @property
    def output_size(self):
        return [1, 1, 1]

    def __init__(self, velocity_units, input_units, dt, activation='relu', **kwargs):
        self.velocity_units = velocity_units
        self.input_units = input_units
        self.dt = dt
        self.activation = activation
        super().__init__(**kwargs)

    @property
    def state_size(self):
        return [1, 1]

    def build(self, input_shape):
        self.velocity_transform = Dense(self.velocity_units, activation=self.activation, use_bias=True,
                                        name='transformed_velocity')

        self.input_transform = Dense(self.input_units, activation=self.activation, use_bias=True,
                                     name='transformed_state')
        self.built = True

    # input_at_t: velocity
    # state_at_t: phi(t), grad_phi(t)
    # outputs: v(phi(t)) (=dt_phi(t+1))
    # state_at_(t+1): phi(t+1), grad_phi(t+1)

    def call(self, inputs, **kwargs):
        input_at_t = inputs
        state_at_t = kwargs['states']
        phi = state_at_t[0]
        grad_phi = state_at_t[1]
        v_phi = self.apply_v_field(phi, input_at_t)
        grad_v_phi = tf.gradients(v_phi, phi)[0]
        phi_new = phi + dt * v_phi
        grad_phi_new = grad_phi + dt * grad_v_phi * grad_phi
        return v_phi, phi_new, grad_phi_new

    def apply_v_field(self, inputs, velocity):
        v_phi_intermediate = Concatenate()([self.input_transform(inputs), self.velocity_transform(velocity)])
        v_phi = Dense(1, activation=None, use_bias=True)(v_phi_intermediate)
        return v_phi


def build_rnn(num_t_vals, num_x_vals, x_vals, velocity_units, input_units, dt, dx, **kwargs):
    input_dx_u = Input(batch_shape=(num_x_vals, num_t_vals, 1), dtype='float', name='dx_u')
    input_dt_u = Input(batch_shape=(num_x_vals, num_t_vals, 1), dtype='float', name='dt_u')
    input_velocity = Input(dtype='float', name='velocity', batch_shape=(num_x_vals, 1))
    input_x = Input(tensor=tf.constant(value=x_vals, dtype='float', shape=(num_x_vals, 1)), name='input_x')

    cell = VFieldRNNCell(velocity_units, input_units, dt, **kwargs)

    phi_list = []

    # initial values: phi = id
    phi = input_x
    grad_phi = Input(tensor=tf.constant(value=1, dtype='float', shape=(num_x_vals, 1)), batch_shape=(num_x_vals, 1))
    loss = 0

    for i in range(1, num_t_vals):
        v_phi_t, phi, grad_phi = cell(input_velocity, states=[phi, grad_phi])
        phi_list.append(phi)
        loss += tf.reduce_sum((input_dt_u[:, i] - v_phi_t * input_dx_u[:, i] / grad_phi) ** 2 * grad_phi) * dt * dx

    model = Model([input_velocity, input_dt_u, input_dx_u], phi_list)
    model.add_loss(loss)
    return model


model = build_rnn(num_t_vals, num_x_vals, x_vals, velocity_units=20, input_units=30, dt=dt, dx=dx, activation='relu')
model.compile(optimizer=optimizers.Adam(lr=0.001))
model.fit([velocity_train, dt_u_train, dx_u_train], batch_size=num_x_vals, epochs=300, shuffle=True,
          validation_data=([velocity_test, dt_u_test, dx_u_test], None), verbose=2)
phi = model.predict([velocity_train, dt_u_train, dx_u_train])

phi_ref = phi_reference(x_vals, t_vals, 3.6)
diff = np.abs(phi[8][:300, 0] - phi_ref[-1])
